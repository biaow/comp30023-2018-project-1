# Author: biao wang
all:httpd

httpd: httpd.c
	${CC}  -pthread -o httpd httpd.c
httpd-debug: httpd.c
	${CC} -DDEBUG -pthread -o httpd httpd.c
clean:
	rm httpd
run:all
	./httpd 8080 ~/comp30023-2018-project-1/website

debug:httpd-debug
	./httpd 8080 ~/comp30023-2018-project-1/website/


